#include "LED.h"

void LED_Init(PORT_ID port, PIN_ID pin) {
    DIO_Init(port, pin, OUTPUT, NO_PULL);
}

void LED_On(PORT_ID port, PIN_ID pin) {
    DIO_Pin_HIGH(port, pin);
}

void LED_Off(PORT_ID port, PIN_ID pin) {
    DIO_Pin_LOW(port, pin);
}

void LED_Toggle(PORT_ID port, PIN_ID pin) {
    DIO_Toggle_Pin(port, pin);
}
