#include "Keypad.h"

void Keypad_Init() {
    DIO_Init(KEYPAD_ROW_1_PORT, KEYPAD_ROW_1_PIN, INPUT, KEYPAD_ROW_1_PULL_RES);
    DIO_Init(KEYPAD_ROW_2_PORT, KEYPAD_ROW_2_PIN, INPUT, KEYPAD_ROW_2_PULL_RES);
    DIO_Init(KEYPAD_ROW_3_PORT, KEYPAD_ROW_3_PIN, INPUT, KEYPAD_ROW_3_PULL_RES);
    DIO_Init(KEYPAD_ROW_4_PORT, KEYPAD_ROW_4_PIN, INPUT, KEYPAD_ROW_4_PULL_RES);
#if KEYPAD_ROWS_SIZE == 5
    DIO_Init(KEYPAD_ROW_5_PORT, KEYPAD_ROW_5_PIN, INPUT, KEYPAD_ROW_5_PULL_RES);
#endif

    DIO_Init(KEYPAD_COLUMN_1_PORT, KEYPAD_COLUMN_1_PIN, OUTPUT, NO_PULL);
    DIO_Init(KEYPAD_COLUMN_2_PORT, KEYPAD_COLUMN_2_PIN, OUTPUT, NO_PULL);
    DIO_Init(KEYPAD_COLUMN_3_PORT, KEYPAD_COLUMN_3_PIN, OUTPUT, NO_PULL);
    DIO_Init(KEYPAD_COLUMN_4_PORT, KEYPAD_COLUMN_4_PIN, OUTPUT, NO_PULL);
}

uint8 Keypad_GetKey() {
    uint8 row, col, key = 0;
    for(col = 0; col < KEYPAD_COLUMNS_SIZE; col++) {
        switch (col) {
            case 0:
                DIO_Pin_LOW(KEYPAD_COLUMN_1_PORT, KEYPAD_COLUMN_1_PIN);
                break;
            case 1:
                DIO_Pin_LOW(KEYPAD_COLUMN_2_PORT, KEYPAD_COLUMN_2_PIN);
                break;
            case 2:
                DIO_Pin_LOW(KEYPAD_COLUMN_3_PORT, KEYPAD_COLUMN_3_PIN);
                break;
            case 3:
                DIO_Pin_LOW(KEYPAD_COLUMN_4_PORT, KEYPAD_COLUMN_4_PIN);
                break;
        }
        for(row = 0; row < KEYPAD_ROWS_SIZE; row++) {
            switch (row) {
                case 0:
                    if(DIO_Read_Pin(KEYPAD_ROW_1_PORT, KEYPAD_ROW_1_PIN) == LOW) {
                        key = mapping[row][col];
                        while(DIO_Read_Pin(KEYPAD_ROW_1_PORT, KEYPAD_ROW_1_PIN) == LOW);
                    }
                    break;
                case 1:
                    if(DIO_Read_Pin(KEYPAD_ROW_2_PORT, KEYPAD_ROW_2_PIN) == LOW) {
                        key = mapping[row][col];
                        while(DIO_Read_Pin(KEYPAD_ROW_2_PORT, KEYPAD_ROW_2_PIN) == LOW);
                    }
                    break;
                case 2:
                    if(DIO_Read_Pin(KEYPAD_ROW_3_PORT, KEYPAD_ROW_3_PIN) == LOW) {
                        key = mapping[row][col];
                        while(DIO_Read_Pin(KEYPAD_ROW_3_PORT, KEYPAD_ROW_3_PIN) == LOW);
                    }
                    break;
                case 3:
                    if(DIO_Read_Pin(KEYPAD_ROW_4_PORT, KEYPAD_ROW_4_PIN) == LOW) {
                        key = mapping[row][col];
                        while(DIO_Read_Pin(KEYPAD_ROW_4_PORT, KEYPAD_ROW_4_PIN) == LOW);
                    }
                    break;
#if KEYPAD_ROWS_SIZE == 5
                case 4:
                    if(DIO_Read_Pin(KEYPAD_ROW_5_PORT, KEYPAD_ROW_5_PIN) == LOW) {
                        key = mapping[row][col];
                        while(DIO_Read_Pin(KEYPAD_ROW_5_PORT, KEYPAD_ROW_5_PIN) == LOW);
                    }
                    break;
#endif
            }
            if(key != 0) return key;
        }
        switch (col) {
            case 0:
                DIO_Pin_HIGH(KEYPAD_COLUMN_1_PORT, KEYPAD_COLUMN_1_PIN);
                break;
            case 1:
                DIO_Pin_HIGH(KEYPAD_COLUMN_2_PORT, KEYPAD_COLUMN_2_PIN);
                break;
            case 2:
                DIO_Pin_HIGH(KEYPAD_COLUMN_3_PORT, KEYPAD_COLUMN_3_PIN);
                break;
            case 3:
                DIO_Pin_HIGH(KEYPAD_COLUMN_4_PORT, KEYPAD_COLUMN_4_PIN);
                break;
        }
    }
    return key;
}

