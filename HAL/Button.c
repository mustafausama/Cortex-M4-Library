#include "Button.h"

void Button_Init(PORT_ID port, PIN_ID pin, PULL_RES pull_res) {
    DIO_Init(port, pin, INPUT, pull_res);
}

BTN_STATE Button_GetState(PORT_ID port, PIN_ID pin, PULL_RES pull_res) {
    LOGIC_LEVEL v = DIO_Read_Pin(port, pin);
    switch (pull_res) {
        case PULLUP:
            return v == LOW ? PRESSED : RELEASED;
        case PULLDOWN:
            return v == HIGH ? RELEASED : PRESSED;
    }
    return RELEASED;
}
