#include "../MCAL/DIO.h"

#ifndef __HAL_LED_H__
#define __HAL_LED_H__

typedef struct {
    PORT_ID port;
    PIN_ID pin;
} DEF_LED;

static DEF_LED LED_RED = {PORTF, PIN1};
static DEF_LED LED_BLUE = {PORTF, PIN2};
static DEF_LED LED_GREEN = {PORTF, PIN3};

void LED_Init(PORT_ID port, PIN_ID pin);

void LED_On(PORT_ID port, PIN_ID pin);

void LED_Off(PORT_ID port, PIN_ID pin);

void LED_Toggle(PORT_ID port, PIN_ID pin);

#endif