#include "../MCAL/types.h"
#include "../MCAL/DIO.h"

#ifndef __HAL_KEYPAD_H__
#define __HAL_KEYPAD_H__

#define KEYPAD_COLUMNS_SIZE 4
#define KEYPAD_ROWS_SIZE 5

#define KEYPAD_ROW_1_PORT PORTF
#define KEYPAD_ROW_1_PIN PIN0
#define KEYPAD_ROW_1_PULL_RES PULLUP
#define KEYPAD_ROW_2_PORT PORTA
#define KEYPAD_ROW_2_PIN PIN2
#define KEYPAD_ROW_2_PULL_RES PULLUP
#define KEYPAD_ROW_3_PORT PORTA
#define KEYPAD_ROW_3_PIN PIN3
#define KEYPAD_ROW_3_PULL_RES PULLUP
#define KEYPAD_ROW_4_PORT PORTA
#define KEYPAD_ROW_4_PIN PIN4
#define KEYPAD_ROW_4_PULL_RES PULLUP
#if KEYPAD_ROWS_SIZE == 5
#define KEYPAD_ROW_5_PORT PORTF
#define KEYPAD_ROW_5_PIN PIN4
#define KEYPAD_ROW_5_PULL_RES PULLUP
#endif

#define KEYPAD_COLUMN_1_PORT PORTE
#define KEYPAD_COLUMN_1_PIN PIN1
#define KEYPAD_COLUMN_2_PORT PORTE
#define KEYPAD_COLUMN_2_PIN PIN2
#define KEYPAD_COLUMN_3_PORT PORTE
#define KEYPAD_COLUMN_3_PIN PIN3
#define KEYPAD_COLUMN_4_PORT PORTE
#define KEYPAD_COLUMN_4_PIN PIN4

#if KEYPAD_ROWS_SIZE == 5
static uint8 mapping[][4] = {
    {'A', 'B', 'C', 'D'},
    {'1', '2', '3', '+'},
    {'4', '5', '6', '-'},
    {'7', '8', '9', '*'},
    {'.', '0', '=', '/'}
};
#else
static uint8 mapping[][4] = {
    {'1', '2', '3', '+'},
    {'4', '5', '6', '-'},
    {'7', '8', '9', '*'},
    {'.', '0', '=', '/'}
};
#endif

void Keypad_Init();

uint8 Keypad_GetKey();

#endif
