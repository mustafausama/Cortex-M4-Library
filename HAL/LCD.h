#include "../MCAL/types.h"
#include "../MCAL/DIO.h"
#include "../MCAL/types.h"

#ifndef __HAL_LCD_H__
#define __HAL_LCD_H__

#define LCD_RS_PORT PORTC
#define LCD_RS_PIN  PIN4

#define LCD_EN_PORT PORTC
#define LCD_EN_PIN  PIN6

#define LCD_MODE 8

#if LCD_MODE == 8

#define LCD_D0_PORT PORTB
#define LCD_D0_PIN  PIN0

#define LCD_D1_PORT PORTB
#define LCD_D1_PIN  PIN1

#define LCD_D2_PORT PORTB
#define LCD_D2_PIN  PIN2

#define LCD_D3_PORT PORTB
#define LCD_D3_PIN  PIN3

#endif

#define LCD_D4_PORT PORTB
#define LCD_D4_PIN  PIN4

#define LCD_D5_PORT PORTB
#define LCD_D5_PIN  PIN5

#define LCD_D6_PORT PORTB
#define LCD_D6_PIN  PIN6

#define LCD_D7_PORT PORTB
#define LCD_D7_PIN  PIN7

#define LCD_ROWS 2
#define LCD_COLS 16

void LCD_Init(void);
void LCD_Clear(void);
void LCD_MoveCursor(uint8 x, uint8 y);
void LCD_DisplayChar(uint8 data);
void LCD_DisplayStr(uint8* str);
void LCD_DisplayInt(uint32 data);

// Extra functions
void LCD_DisplayOn(void);
void LCD_DisplayOff(void);
void LCD_BlinkOn(void);
void LCD_BlinkOff(void);
void LCD_CursorOn(void);
void LCD_CursorOff(void);
void LCD_ScrollLeft(void);
void LCD_ScrollRight(void);
void LCD_CursorLeft(void);
void LCD_CursorRight(void);

#endif
