#include "../MCAL/DIO.h"

#ifndef __HAL_BUTTON_H__
#define __HAL_BUTTON_H__

typedef struct {
    PORT_ID port;
    PIN_ID pin;
} DEF_BTN;

typedef enum {
    PRESSED,
    RELEASED
} BTN_STATE;

static DEF_BTN SW1 = {PORTF, PIN4};
static DEF_BTN SW2 = {PORTF, PIN0};

void Button_Init(PORT_ID port, PIN_ID pin, PULL_RES pull_res);

BTN_STATE Button_GetState(PORT_ID port, PIN_ID pin, PULL_RES pull_res);

#endif