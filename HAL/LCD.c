#include "LCD.h"
#include "../MCAL/delay.h"
#include "../MCAL/bit_operations.h"

// Commands
#define LCD_CMD_CLEAR_DISPLAY   0x01
#define LCD_CMD_FOUR_BIT_MODE   0x28
#define LCD_CMD_EIGHT_BIT_MODE  0x38
#define LCD_CMD_CURSOR_OFF      0x0C
#define LCD_CMD_SET_MOVE_CURSOR 0x80
// Flags
#define LCD_DISPLAYCONTROL      0x08
#define LCD_DISPLAYON           0x04
#define LCD_BLINKON             0x01
#define LCD_CURSORON            0x02
#define LCD_CURSORSHIFT         0x10
#define LCD_DISPLAYMOVE         0x08
#define LCD_MOVELEFT            0x00
#define LCD_MOVERIGHT           0x04

// Private functions
static inline void LCD_SendCommand(uint8 command);
static void LCD_SendCommand_4Bit(uint8 command);
static void LCD_SendCommand_8Bit(uint8 command);

static uint8 current_row = 0;
static uint8 current_col = 0;

static uint8 DisplayControl = LCD_DISPLAYCONTROL;

// Public functions
void LCD_Init(void) {
    DIO_Init(LCD_RS_PORT, LCD_RS_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_EN_PORT, LCD_EN_PIN, OUTPUT, NO_PULL);
    SysTick_Delay(1);

#if LCD_MODE == 8
    DIO_Init(LCD_D0_PORT, LCD_D0_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D1_PORT, LCD_D1_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D2_PORT, LCD_D2_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D3_PORT, LCD_D3_PIN, OUTPUT, NO_PULL);
#endif
    DIO_Init(LCD_D4_PORT, LCD_D4_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D5_PORT, LCD_D5_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D6_PORT, LCD_D6_PIN, OUTPUT, NO_PULL);
    DIO_Init(LCD_D7_PORT, LCD_D7_PIN, OUTPUT, NO_PULL);

#if LCD_MODE == 8
    LCD_SendCommand(LCD_CMD_EIGHT_BIT_MODE);
#elif LCD_MODE == 4
    LCD_SendCommand(LCD_CMD_FOUR_BIT_MODE);
#endif

    LCD_SendCommand(LCD_CMD_CURSOR_OFF);
    LCD_Clear();
    LCD_DisplayOn();
}

void LCD_Clear(void) {
    LCD_SendCommand(LCD_CMD_CLEAR_DISPLAY);
    LCD_MoveCursor(0, 0);
}

void LCD_MoveCursor(uint8 r, uint8 c) {
    uint8 move_command = LCD_CMD_SET_MOVE_CURSOR;

    switch(r) {
        case 0:
            move_command |= c;
            break;
        case 1:
            move_command |= 0x40 + c;
            break;
        case 2:
            move_command |= 0x10 + c;
            break;
        case 3:
            move_command |= 0x50 + c;
            break;
    }

    LCD_SendCommand(move_command);
    current_row = r;
    current_col = c;
}

void LCD_DisplayChar(uint8 data) {
    if(current_col >= LCD_COLS - 1)
        LCD_ScrollLeft();
    DIO_Write_Pin(LCD_RS_PORT, LCD_RS_PIN, HIGH);
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, HIGH);
    SysTick_Delay(1);

#if LCD_MODE == 8
    DIO_Write_Pin(LCD_D0_PORT, LCD_D0_PIN, GET_BIT(data, 0));
    DIO_Write_Pin(LCD_D1_PORT, LCD_D1_PIN, GET_BIT(data, 1));
    DIO_Write_Pin(LCD_D2_PORT, LCD_D2_PIN, GET_BIT(data, 2));
    DIO_Write_Pin(LCD_D3_PORT, LCD_D3_PIN, GET_BIT(data, 3));
    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(data, 4));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(data, 5));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(data, 6));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(data, 7));
#elif LCD_MODE == 4
    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(data, 4));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(data, 5));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(data, 6));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(data, 7));

    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, LOW);
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, HIGH);

    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(data, 0));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(data, 1));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(data, 2));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(data, 3));
#endif
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, LOW);
    SysTick_Delay(1);
    current_col++;
}

void LCD_DisplayStr(uint8* str) {
    while(*str != '\0') {
        LCD_DisplayChar(*str);
        str++;
    }
}

void LCD_DisplayInt(uint32 data) {
    uint8 str[11];
    uint8 i = 0;
    while(data != 0) {
        str[i] = data % 10 + '0';
        data /= 10;
        i++;
    }
    str[i] = '\0';
    for(uint8 j = 0; j < i / 2; j++) {
        uint8 temp = str[j];
        str[j] = str[i - j - 1];
        str[i - j - 1] = temp;
    }
    LCD_DisplayStr(str);
}

// Extra functions
void LCD_DisplayOn(void) {
    DisplayControl |= LCD_DISPLAYON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_DisplayOff(void) {
    DisplayControl &= ~LCD_DISPLAYON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_BlinkOn(void) {
    DisplayControl |= LCD_BLINKON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_BlinkOff(void) {
    DisplayControl &= ~LCD_BLINKON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_CursorOn(void) {
    DisplayControl |= LCD_CURSORON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_CursorOff(void) {
    DisplayControl &= ~LCD_CURSORON;
    LCD_SendCommand(LCD_DISPLAYCONTROL | DisplayControl);
}

void LCD_ScrollLeft(void) {
    LCD_SendCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void LCD_ScrollRight(void) {
    LCD_SendCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void LCD_CursorLeft(void) {
    if(current_col > 0)
        LCD_MoveCursor(current_row, current_col - 1);
}

void LCD_CursorRight(void) {
    if(current_col >= LCD_COLS-1)
        LCD_ScrollLeft();
    LCD_MoveCursor(current_row, current_col + 1);
}

// Private functions implementation

void LCD_SendCommand(uint8 command) {
#if LCD_MODE == 8
    LCD_SendCommand_8Bit(command);
#elif LCD_MODE == 4
    LCD_SendCommand_4Bit(command);
#endif
}

void LCD_SendCommand_4Bit(uint8 command) {
    DIO_Write_Pin(LCD_RS_PORT, LCD_RS_PIN, LOW);
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, HIGH);
    SysTick_Delay(1);    

    // Send the higher nibble
    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(command, 4));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(command, 5));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(command, 6));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(command, 7));

    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, LOW);
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, HIGH);
    SysTick_Delay(1);

    // Send the lower nibble
    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(command, 0));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(command, 1));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(command, 2));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(command, 3));

    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, LOW);
    SysTick_Delay(1);
}

void LCD_SendCommand_8Bit(uint8 command) {
    DIO_Write_Pin(LCD_RS_PORT, LCD_RS_PIN, LOW);
    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, HIGH);
    SysTick_Delay(1);

    DIO_Write_Pin(LCD_D0_PORT, LCD_D0_PIN, GET_BIT(command, 0));
    DIO_Write_Pin(LCD_D1_PORT, LCD_D1_PIN, GET_BIT(command, 1));
    DIO_Write_Pin(LCD_D2_PORT, LCD_D2_PIN, GET_BIT(command, 2));
    DIO_Write_Pin(LCD_D3_PORT, LCD_D3_PIN, GET_BIT(command, 3));
    DIO_Write_Pin(LCD_D4_PORT, LCD_D4_PIN, GET_BIT(command, 4));
    DIO_Write_Pin(LCD_D5_PORT, LCD_D5_PIN, GET_BIT(command, 5));
    DIO_Write_Pin(LCD_D6_PORT, LCD_D6_PIN, GET_BIT(command, 6));
    DIO_Write_Pin(LCD_D7_PORT, LCD_D7_PIN, GET_BIT(command, 7));

    SysTick_Delay(1);
    DIO_Write_Pin(LCD_EN_PORT, LCD_EN_PIN, LOW);
    SysTick_Delay(1);
}
