#ifndef _BIT_OPERATIONS_H
#define _BIT_OPERATIONS_H

#define OR_EQ(reg, value) reg |= value
#define AND_EQ(reg, value) reg &= value
#define XOR_EQ(reg, value) reg ^= value

#define AND_BW(reg, val) (reg & val)
#define OR_BW(reg, val) (reg & val)
#define NOT(val) (!val)
#define INV(reg) (~reg)

#define U1 0x01U
#define SL(num, bits) (num << bits)
#define SR(num, bits) (num >> bits)

#define SET_BIT(reg, bit) OR_EQ(reg, SL(U1, bit))
#define CLEAR_BIT(reg, bit) AND_EQ(reg, INV(SL(U1, bit)))
#define TOGGLE_BIT(reg, bit) XOR_EQ(reg, (SL(U1, bit)))

#define GET_BIT(reg, bit) AND_BW(U1, SR(reg, bit))
#define IS_BIT_SET(reg, bit) SR(AND_BW(SL(U1, bit), reg), bit)
#define IS_BIT_CLEARED(reg, bit) NOT(IS_BIT_SET(reg, bit))

#endif