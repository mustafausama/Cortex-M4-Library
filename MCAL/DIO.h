#include "types.h"

#ifndef _DIO_H
#define _DIO_H

#define NUM_PINS 8

typedef enum {
    PIN0,
    PIN1,
    PIN2,
    PIN3,
    PIN4,
    PIN5,
    PIN6,
    PIN7
} PIN_ID;

typedef enum {
    PORTA,
    PORTB,
    PORTC,
    PORTD,
    PORTE,
    PORTF
} PORT_ID;

typedef enum {
    LOW,
    HIGH
} LOGIC_LEVEL;

typedef enum {
    INPUT,
    OUTPUT
} LOGIC_DIR;

typedef enum {
    NO_PULL,
    PULLUP,
    PULLDOWN
} PULL_RES;


void DIO_Init(PORT_ID port, PIN_ID pin, LOGIC_DIR dir, PULL_RES pullup);

void DIO_Write_Port(PORT_ID port, uint8 value);

void DIO_Write_Pin(PORT_ID port, PIN_ID pin, LOGIC_LEVEL value);

void DIO_Pin_HIGH(PORT_ID port, PIN_ID pin);

void DIO_Pin_LOW(PORT_ID port, PIN_ID pin);

uint8 DIO_Read_Port(PORT_ID port);

LOGIC_LEVEL DIO_Read_Pin(PORT_ID port, PIN_ID pin);

void DIO_Toggle_Pin(PORT_ID port, PIN_ID pin);

#endif
