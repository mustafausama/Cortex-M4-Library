#include "delay.h"

void SysTickDisable(void) {
    NVIC_ST_CTRL_R &= ~(NVIC_ST_CTRL_ENABLE);
}

void SysTickEnable(void) {
    NVIC_ST_CTRL_R |= (NVIC_ST_CTRL_CLK_SRC | NVIC_ST_CTRL_ENABLE);
}

uint32 SysTickPeriodGet(void) {
    uint32 reload = NVIC_ST_RELOAD_R & 0x0FFFFFF;
    return (reload + 1) / PROCESSOR_FREQUENCY * 1000;
}

void SysTickPeriodset(uint32 ui32Period) {
    uint32 reload = (ui32Period * (PROCESSOR_FREQUENCY / 1000)) - 1;
    if (reload > 0x0FFFFFF) reload = 0x0FFFFFF;
    NVIC_ST_RELOAD_R = reload;
}

uint32 SysTickValueGet(void){
    return NVIC_ST_CURRENT_R & 0x00FFFFFF;
}

bool SysTickIsTimout(void){
    if ((NVIC_ST_CTRL_R & NVIC_ST_CTRL_COUNT) == 0)
        return False;
    return True;
}

void SysTick_Delay(uint32 delay) {
    SysTickPeriodset(delay);
    while(!SysTickIsTimout());
}