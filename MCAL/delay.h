#include "tm4c123gh6pm.h"
#include "types.h"

#ifndef _DELAY_H_
#define _DELAY_H_

#define PROCESSOR_FREQUENCY 16000000

void SysTickDisable(void);
void SysTickEnable(void);
uint32 SysTickPeriodGet(void);
void SysTickPeriodset(uint32 ui32Period);
uint32 SysTickValueGet(void);
bool SysTickIsTimout(void);

void SysTick_Delay(uint32 delay);

#endif