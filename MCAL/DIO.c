#include "DIO.h"
#include "tm4c123gh6pm.h"
#include "bit_operations.h"

// Initialize <pin> in <port> as <dir> with <pullup> resistor
void DIO_Init(PORT_ID port, PIN_ID pin, LOGIC_DIR dir, PULL_RES pullup) {
    SYSCTL_RCGCGPIO_R |= (1 << port);
    while(SYSCTL_PRGPIO_R & (1 << port) == 0);

    switch(port) {
        case PORTA:
            // Unlocking the GPIOCR register
            GPIO_PORTA_LOCK_R = 0x4C4F434B;

            // Committing the changes to the GPIOCR register
            SET_BIT(GPIO_PORTA_CR_R, pin);

            // Setting the direction of the pin
            // if dir = 1, only the first statement will affect the register
            // if dir = 0, only the second statement will affect the register
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTA_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTA_DIR_R, pin);

            // Setting the pullup resistor if needed
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTA_PUR_R, pin);

            // Setting the pulldown resistor if needed
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTA_PDR_R, pin);

            // Setting the digital enable
            SET_BIT(GPIO_PORTA_DEN_R, pin);
            break;

        case PORTB:
            GPIO_PORTB_LOCK_R = 0x4C4F434B;
            SET_BIT(GPIO_PORTB_CR_R, pin);
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTB_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTB_DIR_R, pin);
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTB_PUR_R, pin);
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTB_PDR_R, pin);
            SET_BIT(GPIO_PORTB_DEN_R, pin);
            break;

        case PORTC:
            GPIO_PORTC_LOCK_R = 0x4C4F434B;
            SET_BIT(GPIO_PORTC_CR_R, pin);
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTC_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTC_DIR_R, pin);
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTC_PUR_R, pin);
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTC_PDR_R, pin);
            SET_BIT(GPIO_PORTC_DEN_R, pin);
            break;

        case PORTD:
            GPIO_PORTD_LOCK_R = 0x4C4F434B;
            SET_BIT(GPIO_PORTD_CR_R, pin);
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTD_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTD_DIR_R, pin);
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTD_PUR_R, pin);
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTD_PDR_R, pin);
            SET_BIT(GPIO_PORTD_DEN_R, pin);
            break;

        case PORTE:
            GPIO_PORTE_LOCK_R = 0x4C4F434B;
            SET_BIT(GPIO_PORTE_CR_R, pin);
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTE_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTE_DIR_R, pin);
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTE_PUR_R, pin);
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTE_PDR_R, pin);
            SET_BIT(GPIO_PORTE_DEN_R, pin);
            break;

        case PORTF:
            GPIO_PORTF_LOCK_R = 0x4C4F434B;
            SET_BIT(GPIO_PORTF_CR_R, pin);
            if(dir == OUTPUT)
                SET_BIT(GPIO_PORTF_DIR_R, pin);
            else
                CLEAR_BIT(GPIO_PORTF_DIR_R, pin);
            if(pullup == PULLUP)
                SET_BIT(GPIO_PORTF_PUR_R, pin);
            if(pullup == PULLDOWN)
                SET_BIT(GPIO_PORTF_PDR_R, pin);
            SET_BIT(GPIO_PORTF_DEN_R, pin);
            break;
    }
}

// Write <value> to <port> bitwise
void DIO_Write_Port(PORT_ID port, uint8 value) {
    switch(port) {
        case PORTA:
            GPIO_PORTA_DATA_R = value;
            break;
        case PORTB:
            GPIO_PORTB_DATA_R = value;
            break;
        case PORTC:
            GPIO_PORTC_DATA_R = value;
            break;
        case PORTD:
            GPIO_PORTD_DATA_R = value;
            break;
        case PORTE:
            GPIO_PORTE_DATA_R = value;
            break;
        case PORTF:
            GPIO_PORTF_DATA_R = value;
            break;
    }
}

// Write <value> to <pin> in <port>
void DIO_Write_Pin(PORT_ID port, PIN_ID pin, LOGIC_LEVEL value) {
    switch(port) {
        case PORTA:
            if(value == HIGH)
                OR_EQ(GPIO_PORTA_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTA_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTB:
            if(value == HIGH)
                OR_EQ(GPIO_PORTB_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTB_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTC:
            if(value == HIGH)
                OR_EQ(GPIO_PORTC_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTC_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTD:
            if(value == HIGH)
                OR_EQ(GPIO_PORTD_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTD_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTE:
            if(value == HIGH)
                OR_EQ(GPIO_PORTE_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTE_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTF:
            if(value == HIGH)
                OR_EQ(GPIO_PORTF_DATA_R, SL(U1, pin));
            else
                AND_EQ(GPIO_PORTF_DATA_R, INV(SL(U1, pin)));
            break;
    }
}

// Set <pin> in <port> to HIGH
void DIO_Pin_HIGH(PORT_ID port, PIN_ID pin) {
    switch(port) {
        case PORTA:
            OR_EQ(GPIO_PORTA_DATA_R, SL(U1, pin));
            break;
        case PORTB:
            OR_EQ(GPIO_PORTB_DATA_R, SL(U1, pin));
            break;
        case PORTC:
            OR_EQ(GPIO_PORTC_DATA_R, SL(U1, pin));
            break;
        case PORTD:
            OR_EQ(GPIO_PORTD_DATA_R, SL(U1, pin));
            break;
        case PORTE:
            OR_EQ(GPIO_PORTE_DATA_R, SL(U1, pin));
            break;
        case PORTF:
            OR_EQ(GPIO_PORTF_DATA_R, SL(U1, pin));
            break;
    }
}

// Set <pin> in <port> to LOW
void DIO_Pin_LOW(PORT_ID port, PIN_ID pin) {
    switch(port) {
        case PORTA:
            AND_EQ(GPIO_PORTA_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTB:
            AND_EQ(GPIO_PORTB_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTC:
            AND_EQ(GPIO_PORTC_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTD:
            AND_EQ(GPIO_PORTD_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTE:
            AND_EQ(GPIO_PORTE_DATA_R, INV(SL(U1, pin)));
            break;
        case PORTF:
            AND_EQ(GPIO_PORTF_DATA_R, INV(SL(U1, pin)));
            break;
    }
}

// Read <port> bitwise
uint8 DIO_Read_Port(PORT_ID port) {
    switch(port) {
        case PORTA:
            return GPIO_PORTA_DATA_R;
        case PORTB:
            return GPIO_PORTB_DATA_R;
        case PORTC:
            return GPIO_PORTC_DATA_R;
        case PORTD:
            return GPIO_PORTD_DATA_R;
        case PORTE:
            return GPIO_PORTE_DATA_R;
        case PORTF:
            return GPIO_PORTF_DATA_R;
    }
}

// Read <pin> in <port>
LOGIC_LEVEL DIO_Read_Pin(PORT_ID port, PIN_ID pin) {
    switch(port) {
        case PORTA:
            return GET_BIT(GPIO_PORTA_DATA_R, pin);
        case PORTB:
            return GET_BIT(GPIO_PORTB_DATA_R, pin);
        case PORTC:
            return GET_BIT(GPIO_PORTC_DATA_R, pin);
        case PORTD:
            return GET_BIT(GPIO_PORTD_DATA_R, pin);
        case PORTE:
            return GET_BIT(GPIO_PORTE_DATA_R, pin);
        case PORTF:
            return GET_BIT(GPIO_PORTF_DATA_R, pin);
    }
}

// Toggle <pin> in <port>
void DIO_Toggle_Pin(PORT_ID port, PIN_ID pin) {
    switch(port) {
        case PORTA:
            XOR_EQ(GPIO_PORTA_DATA_R, SL(U1, pin));
            break;
        case PORTB:
            XOR_EQ(GPIO_PORTB_DATA_R, SL(U1, pin));
            break;
        case PORTC:
            XOR_EQ(GPIO_PORTC_DATA_R, SL(U1, pin));
            break;
        case PORTD:
            XOR_EQ(GPIO_PORTD_DATA_R, SL(U1, pin));
            break;
        case PORTE:
            XOR_EQ(GPIO_PORTE_DATA_R, SL(U1, pin));
            break;
        case PORTF:
            XOR_EQ(GPIO_PORTF_DATA_R, SL(U1, pin));
            break;
    }
}
