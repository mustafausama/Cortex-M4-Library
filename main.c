#include "HAL/Keypad.h"
#include "HAL/LCD.h"
#include "MCAL/delay.h"

void main() {
    SysTickEnable();
    Keypad_Init();
    LCD_Init();
    LCD_Clear();
    uint8 key = Keypad_GetKey(), op;
    key = Keypad_GetKey();
    while(key < '0' || key > '9')
        key = Keypad_GetKey();
    uint32 num1 = key - '0', num2;
    LCD_DisplayChar(key);

    while (1) {
        key = Keypad_GetKey();
        while(key != '+' && key != '-' && key != '*' && key != '/') {
            if(key >= '0' && key <= '9') {
                LCD_DisplayChar(key);
                num1 = num1 * 10 + (key - '0');
            }
            key = Keypad_GetKey();
        }
        op = key;
        LCD_DisplayChar(key);

        key = Keypad_GetKey();
        while(key < '0' || key > '9')
            key = Keypad_GetKey();
        num2 = key - '0';
        LCD_DisplayChar(key);

        key = Keypad_GetKey();
        while(key != '=') {
            if(key >= '0' && key <= '9') {
                LCD_DisplayChar(key);
                num2 = num2 * 10 + (key - '0');
            }
            key = Keypad_GetKey();
        }
        LCD_DisplayChar(key);

        switch(op) {
            case '+':
                num1 += num2;
                break;
            case '-':
                num1 -= num2;
                break;
            case '*':
                num1 *= num2;
                break;
            case '/':
                num1 /= num2;
                break;
        }
        LCD_Clear();
        LCD_DisplayInt(num1);
    }
}
